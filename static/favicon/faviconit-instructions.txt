thanks for using faviconit!
copy the files to your site and add this code inside the HTML <HEAD> tag:

	<!-- ****** faviconit.com favicons ****** -->
	<link rel="shortcut icon" href="/favicon/giorgio.azzinna.ro.ico">
	<link rel="icon" sizes="16x16 32x32 64x64" href="/favicon/giorgio.azzinna.ro.ico">
	<link rel="icon" type="image/png" sizes="196x196" href="/favicon/giorgio.azzinna.ro-192.png">
	<link rel="icon" type="image/png" sizes="160x160" href="/favicon/giorgio.azzinna.ro-160.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon/giorgio.azzinna.ro-96.png">
	<link rel="icon" type="image/png" sizes="64x64" href="/favicon/giorgio.azzinna.ro-64.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon/giorgio.azzinna.ro-32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon/giorgio.azzinna.ro-16.png">
	<link rel="apple-touch-icon" href="/favicon/giorgio.azzinna.ro-57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/favicon/giorgio.azzinna.ro-114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/favicon/giorgio.azzinna.ro-72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/favicon/giorgio.azzinna.ro-144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/favicon/giorgio.azzinna.ro-60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/favicon/giorgio.azzinna.ro-120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/favicon/giorgio.azzinna.ro-76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/favicon/giorgio.azzinna.ro-152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/giorgio.azzinna.ro-180.png">
	<meta name="msapplication-TileColor" content="#FFFFFF">
	<meta name="msapplication-TileImage" content="/favicon/giorgio.azzinna.ro-144.png">
	<meta name="msapplication-config" content="/favicon/browserconfig.xml">
	<!-- ****** faviconit.com favicons ****** -->