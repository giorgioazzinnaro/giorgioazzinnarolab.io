---
title: "Creating Conan packages: package manager for C++"
date: 2017-12-01
categories:
- tech
- conan
tags:
- c++
- rocksdb
- artifactory
- package manager
- conan

autoThumbnailImage: true
thumbnailImagePosition: "top"
coverImage: /images/dependencies.jpeg
metaAlignment: center
---

*Libraries are cool:
don't reinvent the wheel,
don't repeat yourself,
compile once run everywhere...
The list of buzzwords could go on!*

*However it's often not long before libraries turn into...
[dependency hell](https://en.wikipedia.org/wiki/Dependency_hell)!*
<!--more-->

How many times your working piece of software stops *working*
because somebody decided that the library you were using was going to change?

And what about your Linux distribution keeping a specific version of it,
and when you send the code over to a friend their distribution offers another,
and again, your program can't be built?

Or when checking out some code from GitHub, and running CMake multiple times,
because it is unclear of what libraries you're supposed to have on your system?

And finally, when working on multiple projects,
which end up requiring different versions of the same library?

<!-- toc -->

# The need for package managers
## System-wide package managers
__Package manager__ is a [broad term](https://en.wikipedia.org/wiki/Package_manager).

When talking about a Linux distribution,
you may be referring to __apt__, __yum__, __pacman__...
or __homebrew__ for macOS.

The idea behind these is very simple:
for instance, you want to install two applications,
one is an __email client__, the other a __web browser__.

Among their features, they both need to provide *HTML parsing* and *rendering*,
and so they use the same library behind the scenes.
Therefore, there is no need to download this library twice,
as it may be shared between the two.

The package manager keeps track of all the libraries needed for a given application,
and of what is currently installed on the system, and acts accordingly.

## Application-level package managers
So let's now turn to a programmer's use case.
During development, developers go shopping,
and pick all the libraries that will ease their job
and improve the development process and the final outcome.

For a few programming languages this is incredibly easy,
think for example [Maven](https://maven.apache.org/) for Java,
[sbt](http://www.scala-sbt.org/) for Scala,
or [npm](https://www.npmjs.com/) for JavaScript.  
You just need to define a list of libraries,
and they will automatically be downloaded from a repository out on the Internet,
specifically at the version you asked for, and with any dependencies they also need.

## What about C++?
The title featured [Conan](https://www.conan.io/),
by now you'll have realised what it is supposed to do.
Just like Maven or sbt or npm, you define dependencies for your C++ project
in a file (unsurprisingly named `conanfile.txt`), which may look like this:

```txt
[requires]
Poco/1.7.8p3@pocoproject/stable

[generators]
cmake
```

I suggest going through the excellent [documentation](http://docs.conan.io/en/latest/index.html)
to get started.

### What could go wrong
Of course, I can see C++ veterans frowning at this.
C++ is portable, binaries are not.
How are you supposed to throw around packages and just expect them to work?
And while you may have ABI compatibility on different Linux distributions,
what about Windows and macOS? Or different architectures altogether?

Conan tries to address all this,
compiled packages are reused only when possible,
meaning somebody else already uploaded a binary for a machine just like yours,
or you already have a compiled version of the given library in your local Conan cache.

Otherwise, compilation is done on your machine (à la [AUR](https://aur.archlinux.org/)),
even using the options you provide! (e.g. *static* or *shared* library).

## What makes a package managers
Clearly, there is only one thing that makes a package manager useful,
which is, a wide user base.
If all developers releasing a new version of their libraries made it available
on [conan-center](https://bintray.com/conan/conan-center) on Bintray,
it would quickly be widespread,
and developing new software would become incredibly easier.

## Creating Conan packages
Again, the [documentation](http://docs.conan.io/en/latest/packaging.html)
is excellent in explaining the process.

To accelerate adoption though, you'll realise dependencies you already rely upon might have to be packaged.
Before doing any work, I suggest you head to the [wishlist](https://github.com/conan-io/wishlist).
Here everybody may suggest libraries to be introduced into the __conan-center__,
and cooperate to make it happen.

Sometimes you'll find out somebody is already on it,
or already has a package ready but only released it on their own repository on Bintray.
This is because the requirements to upload to the __conan-center__ are
[quite strict](http://docs.conan.io/en/latest/packaging/bintray/conan_center_guide.html),
to ensure good quality and working packages are there.

If you'd like to check Conan out, why don't you package your library,
or help doing the same for a library you often use?
