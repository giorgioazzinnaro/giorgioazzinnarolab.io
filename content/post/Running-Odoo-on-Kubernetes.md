---
title: "Running Odoo on Kubernetes"
date: 2017-07-01
categories:
- tech
- kubernetes
tags:
- kubernetes
- docker
- containers
- gke
- odoo
- saas
autoThumbnailImage: false
thumbnailImagePosition: "top"
thumbnailImage: //d1u9biwaxjngwg.cloudfront.net/welcome-to-tranquilpeak/city-750.jpg
coverImage: //d1u9biwaxjngwg.cloudfront.net/welcome-to-tranquilpeak/city.jpg
metaAlignment: center
draft: true
---

*[Odoo](https://www.odoo.com/) is the leading open source ERP.  
Kubernetes is the most renowned container orchestration tool.*

What happens when you mix them?
<!--more-->

<!-- toc -->

# Odoo on Kubernetes

## Requirements
SaaS ERP application


## Challenge
GKE, secure the application, connect to Google Cloud SQL, LetsEncrypt, mount volumes


## Outcome
Hopefully
